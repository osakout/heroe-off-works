const Sequelize = require("sequelize");

let sequelize = new Sequelize('superhero_test_test', 'omarsakout', '', {
    host: 'localhost',
    dialect: 'postgres',
  
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
  });

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.superheroes = require("./superheroes.js")(sequelize, Sequelize);

module.exports = db;