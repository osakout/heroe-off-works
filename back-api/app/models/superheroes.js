
module.exports = (sequelize, Sequelize) => {
    const Superheroes =
        sequelize.define('superheroes', {
            id: { type: Sequelize.UUID, primaryKey: true, defaultValue: Sequelize.UUIDV4, },
            name: Sequelize.STRING(1000),
            xp: Sequelize.INTEGER,
            location: Sequelize.STRING(1000),
            picturePath: Sequelize.STRING(1000),
            description: Sequelize.STRING(1000),
            skills: Sequelize.ARRAY(Sequelize.TEXT)
        });

    return Superheroes;
}