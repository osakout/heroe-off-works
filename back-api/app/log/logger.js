const winston = require('winston');
const { format } = require('winston');
const colorizer = winston.format.colorize();
const { combine } = format;

const logger = winston.createLogger({
  level: 'info',
  format: combine(
    winston.format.timestamp(),
    winston.format.simple(),
    winston.format.printf(msg =>
        colorizer.colorize(msg.level, `${msg.level}: ${msg.message}`)
    )),
  transports: [
    new winston.transports.Console({
      colorize: true,
    }),
  ],
});
// logger.error('\x1b[36m%s\x1b[0m', 'I am cyan');
module.exports = logger;