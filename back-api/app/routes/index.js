const express = require('express');

const Status = require('../helpers/api-status');
const { respond } = require('../utils/index');
const superheroeAdapter = require('../v1/superheroes/superheroesAdapter');

const router = express.Router();

/* API landing. */
router.get('/', (req, res) => {
  respond(res, { data: {}, meta: Status.WELCOME }, Status.WELCOME.status);
});

router.get('/getAllSuperHeroes', async (req, res) => {
  const allSuperHeroes = await superheroeAdapter.findAllSuperheroes();
  res.send({ SUCCESS: 'SUCCESS', data: allSuperHeroes });
});

router.post('/createSuperHeroe', async (req, res) => {
  const {name, description, xp, location, picturePath, skills} = req.body;

  const data = {
    name: name,
    description: description,
    xp: xp,
    location: location,
    picturePath: picturePath,
    skills: skills
  };

  const superHeroeCreated = await superheroeAdapter.createSuperHeroe(data);
  res.send({ SUCCESS: 'SUCCESS', data: superHeroeCreated.toJSON() });
});


router.delete('/deleteSuperheroById/:id', async (req, res) => {
  const {id} = req.params;

  const superHeroeDestroyed = await superheroeAdapter.deleteSuperheroesById(id);
  res.send({ SUCCESS: 'SUCCESS', deleted: superHeroeDestroyed });
});

module.exports = router;
