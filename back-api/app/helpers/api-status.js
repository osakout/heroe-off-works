module.exports = {
  WELCOME:
  {
    code: 'WELCOME_MESSAGE',
    status: 200,
    title: 'Welcome to our API!',
    detail: 'We salute you.',
  },
  OK:
  {
    code: 'OK',
    status: 200,
    title: 'Success',
    detail: 'Request Success',
  },
  CREATED:
  {
    code: 'CREATED',
    status: 201,
    title: 'Created',
    detail: 'Successfully created',
  },
  NOT_LOGGED_IN:
  {
    code: 'NOT_LOGGED_IN',
    status: 401,
    title: 'Unauthorized',
    detail: 'You are unauthorized to do this',
  },
  NOT_FOUND:
  {
    code: 'NOT_FOUND',
    status: 404,
    title: 'Not Found',
    detail: 'Ressources Not Found',
  },
  INTERNAL_SERVER_ERROR:
  {
    code: 'INTERNAL_SERVER_ERROR',
    status: 500,
    title: 'Internal Server Error',
    detail: 'Internal Server Error',
  },
  FAILED_TOKEN:
  {
  	code: 'FAILED_TOKEN',
  	status: 500,
  	title: 'Token',
  	detail: 'Failed to authenticate token.',
  },
  UNAUTHORIZED_MAIL:
  {
  	code: 'UNAUTHORIZED',
  	status: 401,
  	title: 'Email',
  	detail: 'No email provided.',
  },
  UNAUTHORIZED_PASSWORD:
  {
  	code: 'UNAUTHORIZED',
  	status: 401,
  	title: 'PASSWORD',
  	detail: 'No password provided.',
  },
  FORBIDDEN:
  {
  	code: 'FORBIDDEN',
  	status: 403,
  	title: 'User',
  	detail: 'Wrong user',
  },
  UNAUTHORIZED:
  {
  	code: 'UNAUTHORIZED',
  	status: 401,
  	title: 'Token',
  	detail: 'No token provided.',
  },
};
