const db = require('../../models');
const logger = require('../../log/logger');

class Superheroes {
    async createSuperheroes(superHeroeData) {
        try {
            const superheroes = await db.superheroes.create({
                name: superHeroeData.name,
                description: superHeroeData.description,
                xp: superHeroeData.xp,
                location: superHeroeData.location,
                picturePath: superHeroeData.picturePath,
                skills: superHeroeData.skills
            });
            logger.info(`superHeroe successfully created in createSuperheroes() [superheroesRepository.js]\n${JSON.stringify(superheroes)}`);
            return superheroes;
        } catch (error) {
            logger.error(`Error while creating superHeroe in createSuperheroes() [superheroesRepository.js]\n${error}`);
            throw error;
        }
    }

    async findAllSuperheroes() {
        try {
            const superheroes = await db.superheroes.findAll({});
            logger.info(`superHeroes successfully found in findAllSuperheroes() [superheroesRepository.js]\n${JSON.stringify(superheroes)}`);
            return superheroes;
        } catch (error) {
            logger.error(`Error while creating superHeroe in findAllSuperheroes() [superheroesRepository.js]\n${error}`);
            throw error;
        }
    }

    async findSomeSuperHeroes() {
        try {
            const superheroes = await db.superheroes.findAll({limit: 6});
            logger.info(`superHeroes successfully found in findSomeSuperHeroes() [superheroesRepository.js]\n${JSON.stringify(superheroes)}`);
            return superheroes;
        } catch (error) {
            logger.error(`Error while creating superHeroe in findSomeSuperHeroes() [superheroesRepository.js]\n${error}`);
            throw error;
        }
    }

    async findOneSuperheroe() {
        try {
            const superheroe = await db.superheroes.findOne({});
            logger.info(`superHeroe successfully found in findAllSuperheroes() [superheroesRepository.js]\n${JSON.stringify(superheroe)}`);
            return superheroe;
        } catch (error) {
            logger.error(`Error while creating superHeroe in findAllSuperheroes() [superheroesRepository.js]\n${error}`);
            throw error;
        }
    }

    async deleteSuperheroesById(id) {
        try {
            const superheroe = await db.superheroes.destroy({
                where: { id }
            })
            logger.info(`superHeroe successfully destroyed in deleteSuperheroesById() [superheroesRepository.js]\n${JSON.stringify(superheroes)}`);
            return superheroe;
        } catch (error) {
            logger.error(`Error while destroying superHeroe in deleteSuperheroesById() [superheroesRepository.js]\n${error}`);
            throw error;
        }
    }
}

const superheroes = new Superheroes();

module.exports = superheroes;
