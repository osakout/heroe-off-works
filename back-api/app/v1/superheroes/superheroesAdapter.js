const superheroesRepository = require('./superheroesRepository');
const logger = require('../../log/logger');

class SuperheroesAdapter {
  async createSuperHeroe(superHeroeData) {
    try {
      const superheroes = await superheroesRepository.createSuperheroes(superHeroeData);
      logger.info(`superheroe successfully created in createSuperHeroe() [SuperheroesAdapter.js]\n${JSON.stringify(superheroes)}`);
      return superheroes;
    } catch (error) {
      logger.error(`Error in createSuperHeroe() [SuperheroesAdapter.js] ${error}`);
      return error;
    }
  }

  async findAllSuperheroes() {
    try {
      const superheroes = await superheroesRepository.findAllSuperheroes();
      logger.info(`superheroes successfully found in findAllSuperheroes() [SuperheroesAdapter.js]\n${JSON.stringify(superheroes)}`);
      return superheroes;
    } catch (error) {
      logger.error(`Error in findAllSuperheroes() [SuperheroesAdapter.js] ${error}`);
      return error;
    }
  }

  async findOneSuperheroe() {
    try {
      const superheroe = await superheroesRepository.findOneSuperheroe();
      logger.info(`superheroe successfully found in findOneSuperheroe() [SuperheroesAdapter.js]\n${JSON.stringify(superheroe)}`);
      return superheroe;
    } catch (error) {
      logger.error(`Error in findAllSuperheroes() [SuperheroesAdapter.js] ${error}`);
      return error;
    }
  }

  async findSomeSuperHeroes() {
    try {
      const superheroes = await superheroesRepository.findSomeSuperHeroes();
      logger.info(`superheroes successfully found in findSomeSuperHeroes() [SuperheroesAdapter.js]\n${JSON.stringify(superheroes)}`);
      return superheroes;
    } catch (error) {
      logger.error(`Error in findSomeSuperHeroes() [SuperheroesAdapter.js] ${error}`);
      return error;
    }
  }

  async deleteSuperheroesById(id) {
    try {
        const superheroe = await superheroesRepository.deleteSuperheroesById(id);
        logger.info(`superHeroe successfully destroyed in deleteSuperheroesById() [SuperheroesAdapter.js]\n${JSON.stringify(superheroe)}`);
        return superheroe;
    } catch (error) {
        logger.error(`Error while destroying superHeroe in deleteSuperheroesById() [SuperheroesAdapter.js]\n${error}`);
        throw error;
    }
}
}

const superheroesAdapter = new SuperheroesAdapter();

module.exports = superheroesAdapter;
