let { buildSchema } = require('graphql');

const schema = buildSchema(`
    type superHeroe {
        id: String
        name: String
        location: String
        xp: Int
        description: String
        picturePath: String
        skills: [String]
    }

    type Query {
        findAll: [superHeroe],
        findOne: superHeroe,
        findSomeHeroes: [superHeroe],
        message: String,
        test: String
    }

    type Mutation {
        createHeroe(name: String
            location: String
            xp: Int
            description: String
            picturePath: String
            skills: [String]): superHeroe,
            deleteHeroe(id: String):Int
    }
`);

module.exports = schema;