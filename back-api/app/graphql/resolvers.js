const superHeroeAdapter = require('../v1/superheroes/superheroesAdapter');
// Root resolver
const resolver = {
    message: () => {
        console.log("mon message");
        return 'Hello World!'
    },
    test: () => {
        return 'mon test'
    },
    findOne: async () => {
        const mysuperHeroe = await superHeroeAdapter.findOneSuperheroe();
        return mysuperHeroe;
    },
    findAll: async () => {
        const mysuperHeroes = await superHeroeAdapter.findAllSuperheroes();
        return mysuperHeroes;
    },
    findSomeHeroes: async () => {
        const mysuperHeroes = await superHeroeAdapter.findSomeSuperHeroes();
        return mysuperHeroes;
    },
    createHeroe: async ({ name, xp, location, description, picturePath, skills}) => {
        const data = {
            name,
            description,
            xp,
            location,
            picturePath,
            skills
          };

        const superHeroeCreated = await superHeroeAdapter.createSuperHeroe(data);
        return superHeroeCreated;
    },
/*     updateHeroe: async ({ name, xp, location, description, picturePath, skills}) => {
        const data = {
            name,
            description,
            xp,
            location,
            picturePath,
            skills
          };

        const superHeroeCreated = await superHeroeAdapter.createSuperHeroe(data);
        return superHeroeCreated;
    }, */
    deleteHeroe: async ({id}) => {

        const superHeroeDeleted = await superHeroeAdapter.deleteSuperheroesById(id);
        return superHeroeDeleted;
    }

};

module.exports = resolver;