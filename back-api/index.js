require('./config/config.js');

const express = require('express');
const validator = require('express-validator');
const bodyParser = require('body-parser');
const winston = require('winston');
const cors = require('cors');
const logger = require('./app/log/logger');
const landing = require('./app/routes');
const Sequelize = require('sequelize');
const mysuperheroes = require('./app/utils/superheroes.json');
const app = express();
const db = require("./app/models");

const { graphqlHTTP } = require('express-graphql');
const queries = require('./app/graphql/resolvers');
const graphqlSchemas = require('./app/graphql/schema');

db.sequelize
  .authenticate()
  .then(function (err) {
    logger.info('Connection has been established successfully.');
  })
  .catch(function (err) {
    logger.error('Unable to connect to the database:', err);
  });

db.sequelize.sync({force:true}).then(() => {
  db.superheroes.bulkCreate(mysuperheroes)
    .then(model => {
      //console.log(model);
    }).catch(error => {
      console.log(error)
    });
});

const corsOptions = {
  origin: '*',
  allowedHeaders: ['Content-Type', 'Authorization'],
  optionsSuccessStatus: 200,
};

// Set logging level
winston.level = process.env.LOG_LEVEL;

app.use(bodyParser.json());

app.use(validator({
  customValidators: {
  },
  customSanitizers: {
  },
}));

app.use(cors(corsOptions));

app.use('/', landing);
app.use('/graphql', graphqlHTTP({
  schema: graphqlSchemas,
  rootValue: queries,
  graphiql: true
}));

process.on('uncaughtException', (err) => {
  logger.info('crit', err.stack);
});

app.listen(process.env.LISTENER_PORT, () => {
  logger.info(`App is listening on http://localhost:${process.env.LISTENER_PORT}/`);
});
