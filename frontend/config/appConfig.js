const path = require("path");

// Configure node-config
process.env.NODE_CONFIG_STRICT_MODE = true;
process.env.NODE_CONFIG_ENV         = process.env.NODE_CONFIG_ENV; // eslint-disable-line no-self-assign
process.env.NODE_CONFIG_DIR         = path.resolve(__dirname, "./app");
const config                        = require("config");

class AppConfig {

	constructor() {
		this.config = config;
	}

	get(key) {
		return this.config.get(key);
	}

	getAll() {
		return config.util.toObject();
	}

	getUrl(type) {
		const host = config[type];
		if (!host) { throw new Error(`appConfig#getUrl | Unknown host "${type}"`); }
		return `${host.protocol}://${host.server}${host.port ? `:${host.port}` : ""}`;
	}

	extendDeep(anotherObject) {
		return config.util.extendDeep({ }, this.getAll(), anotherObject);
	}

}

module.exports = new AppConfig();
