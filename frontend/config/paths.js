const path = require("path");

function resolveApp(relativePath) {
	return path.resolve(__dirname, "./../", relativePath);
}

module.exports = {

	file: {
		entryPoint: resolveApp("src/index.tsx"),
		appHtml: resolveApp("public/index.ejs"),
		tsConfig: resolveApp("tsconfig.json")
	},

	dir: {
		dist: resolveApp("../public"),
		root: resolveApp(""),
		src: resolveApp("src"),
		nodeModules: resolveApp("node_modules"),
		public: resolveApp("public")
	},

	node: {
		babelCore: resolveApp("node_modules/@babel/core")
	}
};
