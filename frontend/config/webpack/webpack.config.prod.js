const { mergeWithCustomize } = require("webpack-merge");
const { customizeArray }     = require("webpack-merge");
const HtmlWebpackPlugin      = require("html-webpack-plugin");
const MiniCssExtractPlugin   = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin      = require("copy-webpack-plugin");
const paths                  = require("../paths");

const mergeStrategy = mergeWithCustomize({
	customizeArray: customizeArray({ "entry.*": "prepend" })
});

module.exports = mergeStrategy(require("./webpack.config.base"), {

	mode: "production",

	// 1048576b => 1mo (build error if process.env.CI === true)
	performance: {
		maxEntrypointSize: 1048576,
		maxAssetSize: 1048576
	},

	output: {
		path: paths.dir.dist,
		filename: "static/js/[name].[hash:8].js",
		chunkFilename: "static/js/[name].[hash:8].chunk.js"
	},

	optimization: {
		splitChunks: {
			chunks: "all"
		}
	},

	module: {
		rules: [
			{
				test: /\.css$/,
				use: [MiniCssExtractPlugin.loader, "css-loader"],
				include: [paths.dir.src]
			}
		]
	},

	plugins: [
		new HtmlWebpackPlugin({
			inject: true,
			template: paths.file.appHtml,
			minify: {
				collapseWhitespace: true,
				minifyJS: true,
				removeComments: true,
				removeRedundantAttributes: true,
				removeScriptTypeAttributes: true,
				removeStyleLinkTypeAttributes: true,
				useShortDoctype: true
			}
		}),

		new MiniCssExtractPlugin({
			filename: "static/css/[name].[hash].css",
			chunkFilename: "static/css/[name].[id].[hash].css"
		}),

		new CleanWebpackPlugin({
			verbose: false
		}),

		new CopyWebpackPlugin({
			patterns: [
				{
					from: paths.dir.public,
					to: paths.dir.dist,
					globOptions: {
						dot: true,
						// gitignore: true, // https://github.com/webpack-contrib/copy-webpack-plugin/issues/496
						ignore: ["**/index.{html,ejs}", "**/gallery.json"]
					}
				}
			]
		})
	],

	infrastructureLogging: {
		level: "warn"
	}
});
