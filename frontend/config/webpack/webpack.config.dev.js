const webpack                = require("webpack");
const { mergeWithCustomize } = require("webpack-merge");
const { customizeArray }     = require("webpack-merge");
const HtmlWebpackPlugin      = require("html-webpack-plugin");
const paths                  = require("../paths");
const config                 = require("./utils/webpack.define");

const mergeStrategy = mergeWithCustomize({
	customizeArray: customizeArray({ "entry.*": "prepend" })
});

module.exports = mergeStrategy(require("./webpack.config.base"), {

	mode: "development",
	devtool: "inline-module-source-map", // for debugging (or use "cheap-module-eval-source-map")

	devServer: {
		compress: true,
		port: config.getConfig("front.port"),
		clientLogLevel: "none",
		open: config.getConfig("devServer.open"),
		openPage: config.findUrl("front"),
		historyApiFallback: true,
		contentBase: paths.dir.public,
		watchContentBase: true,
		hot: true,
		publicPath: "/",
		quiet: false,
		watchOptions: { ignored: /node_modules/ },
		https: process.env.HTTPS === "true",
		host: process.env.HOST || "localhost",
		disableHostCheck: true,
		overlay: true
	},

	entry: [
		require.resolve("react-hot-loader/patch")
	],

	resolve: {
		alias: {
			"react-dom": "@hot-loader/react-dom"
		}
	},

	output: {
		filename: "static/js/bundle.js"
		// devtoolModuleFilenameTemplate: info => `file://${info.absoluteResourcePath.replace(/\\/g, "/")}`
	},

	module: {
		rules: [
			{
				test: /\.css$/,
				use: ["style-loader", "css-loader"],
				include: [paths.dir.src]
			}
		]
	},

	plugins: [
		new webpack.WatchIgnorePlugin([paths.dir.nodeModules]),
		new webpack.HotModuleReplacementPlugin(),

		new HtmlWebpackPlugin({
			inject: true,
			template: paths.file.appHtml
		})
	]
});
