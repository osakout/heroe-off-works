const appConfig = require("../../appConfig");

module.exports = {

	getBabelOptions() {
		const isDev = appConfig.get("env.app") === "development";

		// onlyRemoveTypeImports: See https://github.com/TypeStrong/fork-ts-checker-webpack-plugin#type-only-modules-watching
		return {
			cacheDirectory: true,
			babelrc: false,
			presets: [
				"@babel/preset-env", // see package.json#browserslist
				["@babel/preset-typescript", { onlyRemoveTypeImports: true }],
				"@babel/preset-react"
			],
			plugins: [
				["@babel/plugin-proposal-class-properties", { loose: true }],
				"@babel/plugin-proposal-nullish-coalescing-operator",
				"@babel/plugin-transform-runtime",
				"@babel/plugin-proposal-optional-chaining",
				"@babel/plugin-syntax-dynamic-import",
				["babel-plugin-styled-components", { ssr: false, transpileTemplateLiterals: true, displayName: false }],
				...(isDev ? ["react-hot-loader/babel"] : [])
			]
		};
	},

	getBabelOptionsNodeModules() {
		return {
			cacheDirectory: true,
			babelrc: false,
			presets: ["@babel/preset-env"],
			plugins: ["@babel/plugin-transform-modules-commonjs"]
		};
	}

};
