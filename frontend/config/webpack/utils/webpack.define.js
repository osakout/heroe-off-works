const os            = require("os");
const { transform } = require("lodash");
const appConfig     = require("../../appConfig");

module.exports = {

	getConfig(key) {
		return key ? appConfig.get(key) : appConfig.getAll();
	},

	getAppConfig() {
		const env = appConfig.get("env");

		// Update server IP if HOST is set to 0.0.0.0
		const ip = this.findIp(process.env.HOST);
		const conf = appConfig.extendDeep({ api: { server: ip }, front: { server: ip } });

		return {
			__CONFIG__: JSON.stringify(conf),
			"process.env": {
				NODE_ENV: JSON.stringify(env.node),
				GRAPHQL_URL: JSON.stringify(this.findUrl("api", "/graphql")),
				FRONT_URL: JSON.stringify(this.findUrl("front"))
			}
		};
	},

	findUrl(type, path = "") {
		const host   = appConfig.get(type);
		const server = this.findIp(process.env.HOST || host.server);
		return `${host.protocol}://${server}${host.port ? `:${host.port}` : ""}${path}`;
	},

	// Find external IPv4 address (0.0.0.0. => 192.168.1.XXX)
	findIp(host = "localhost") {
		if (host !== "0.0.0.0") { return host; }

		const { ip } = transform(os.networkInterfaces(), (res, ni) => {
			const externalIface = ni.find(iface => iface.family === "IPv4" && !iface.internal);
			if (externalIface) {
				res.ip = externalIface.address;
				return false;
			}
		}, { ip: host });

		return ip;
	}

};
