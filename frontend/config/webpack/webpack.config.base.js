const webpack             = require("webpack");
const PathWebpackPlugin   = require("case-sensitive-paths-webpack-plugin");
const ForkTsCheckerPlugin = require("fork-ts-checker-webpack-plugin");
const babelConfig         = require("./utils/webpack.babel");
const config              = require("./utils/webpack.define");
const paths               = require("../paths");

module.exports = {

	context: paths.dir.src,

	entry: [
		paths.file.entryPoint
	],

	output: {
		path: paths.dir.dist,
		publicPath: "/"
	},

	resolve: {
		extensions: [".js", ".json", ".jsx", ".ts", ".tsx"],
		alias: {
			"#": paths.dir.src
		}
	},

	module: {
		rules: [
			{
				test: /\.(ttf|otf|eot|svg|woff|woff2)$/,
				loader: "file-loader",
				include: [paths.dir.src],
				options: {
					name: "static/media/[name].[hash:8].[ext]"
				}
			},
			{
				test: /\.(bmp|gif|jpg|png)$/,
				loader: "url-loader",
				include: [paths.dir.src],
				options: {
					limit: 10000,
					name: "static/media/[name].[hash:8].[ext]"
				}
			},
			{
				test: /\.(j|t)sx?$/,
				exclude: /node_modules/,
				include: [paths.dir.src],
				loader: "babel-loader",
				options: babelConfig.getBabelOptions()
			}
		]
	},

	plugins: [
		new PathWebpackPlugin(),
		new webpack.DefinePlugin(config.getAppConfig()),

		// Typescript type checking in a separate process
		new ForkTsCheckerPlugin({
			typescript: {
				configFile: paths.file.tsConfig
			},
			async: false
		})
	]

};
