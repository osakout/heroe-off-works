import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import "./styles.css";
import axios from 'axios';

const Create = () => {
	const [name, setName] = useState("");
	const [xp, setXp] = useState("");
	const [location, setLocation] = useState("");
	const [picture, setPicture] = useState("/gallery/off-works.png");
	const [skills, setSkills] = useState("");
	const [description, setDescription] = useState("");
	let history = useHistory();

	function redirectHome() {
		history.push('/home');
	}

	function createHeroe() {
		axios({
			url: 'http://localhost:3000/graphql',
			method: 'post',
			data: {
				query: `    mutation {
					createHeroe(name: "${name}",
						location: "${location}",
						xp: ${xp},
						description: "${description}",
						picturePath: "${picture}",
						skills: ["${skills}"]) {
					name,
					location,
					xp,
					description,
					picturePath,
					skills
				  }
				}`
			}
		}).then((result) => {
			if(result)
			history.push('/home');
		}).catch(error => {
			console.log(error)
		});
	}

	return (
		<>
			<div className="navBar">
				<div className="pathNavBar">
					<p className="Home">Home / </p>
					<p className="Path">⚙️ Add a superhero</p>
				</div>
			</div>

			<div className="containerCreate">

				<div className="cardSuperHeroeCreate">
					<img className="imgSuperHeroeCreate" src={picture} alt="Logo" width="300px" /> {/*  width="50px"  */}
					<div className="superHeroeLabelCreate">
						<p className="superHeroeLabelNameCreate" >
							{name}
						</p>
						<p className="superHeroeLabelSkillsCreate">
							{skills}
						</p>
					</div>
				</div>

				<div className="containerForm">
					<div>
						<span className="inputLabelForm">
							<p className="labelCreateForm">NAME</p>
							<input className="inputForm" type="text" placeholder="My Super Hero" onChange={e => setName(e.target.value)} />
						</span>
						<span className="inputLabelForm">
							<p className="labelCreateForm">XP</p>
							<input className="inputForm" type="text" placeholder="3" onChange={e => setXp(e.target.value)} />
						</span>
						<span className="inputLabelForm">
							<p className="labelCreateForm">LOCATION</p>
							<input className="inputForm" type="text" placeholder="Paris" onChange={e => setLocation(e.target.value)} />
						</span>
						<span className="inputLabelForm">
							<p className="labelCreateForm">PICTURE</p>
							<input className="inputForm" type="text" placeholder={picture} onChange={e => setPicture(e.target.value)} />
						</span>
						<span className="inputLabelForm">
							<p className="labelCreateForm">SKILLS</p>
							<input className="inputForm" type="text" placeholder="My Super Hero" onChange={e => setSkills(e.target.value)} />
						</span>
						<span className="inputLabelForm">
							<p className="labelCreateForm">DESCRIPTION</p>
							<textarea className="inputForm" placeholder="My description" onChange={e => setDescription(e.target.value)} />
						</span>
					</div>
					<div className="validateForm">
						<button className="cancelButtonCreate" onClick={redirectHome}>Cancel</button>
						<button className="validateButtonCreate" onClick={createHeroe}>Validate</button>
					</div>
				</div>

			</div>
		</>
	);
};

export default Create;
