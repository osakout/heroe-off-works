export { Home } from "./home";
export { Profil } from "./profil";
export { Create } from "./create";
export { Edit } from "./edit";
export { Header } from "./header";
export { Footer } from "./footer";