import React, { useState } from "react";

const Footer = () => {
	const [footer] = useState("MY FOOTER");

	return (
		<>
			<p>{footer}</p>
		</>
	);
};

export default Footer;
