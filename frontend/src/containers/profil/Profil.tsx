import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import "./styles.css";
import axios from 'axios';

interface SuperHeroes {
	name: string;
	xp: number;
	location: string;
	picturePath: string;
	description: string;
	skills: [string];
}

const Profil = () => {
	const location = useLocation();
	const [helloWorld] = useState("hello profil");
	const [profil, setProfil] = useState(location.state as any);
	const [someSuperHeroeProfil, setsomeSuperHeroeProfil] = useState<SuperHeroes[]>([]);
	let history = useHistory();

	useEffect(() => {
		axios({
			url: 'http://localhost:3000/graphql',
			method: 'post',
			data: {
				query: `
			  query {
				findSomeHeroes {
					id,
				  name,
				  location,
				  xp,
				  skills,
				  picturePath,
				  description
				}
			  }
				`
			}
		}).then((result) => {
			setsomeSuperHeroeProfil(result.data.data.findSomeHeroes);
		});
	}, [])

	function setViewProfil(data: {}) {
		setProfil(data);
	}

	function deleteProfil(id: string) {
		axios({
			url: 'http://localhost:3000/graphql',
			method: 'post',
			data: {
				query: `
				mutation {
					deleteHeroe(id: "${profil.id}")
				}
				`
			}
		}).then((result) => {
			if (result.data.data.deleteHeroe === 1)
				history.push('/home');
		}).catch(error => {
			console.log(error)
		});
	}

	return (
		<>
			<div className="navBar">
				<div className="pathNavBar">
					<p className="Home">Home / </p>
					<p className="Path">🖨Superhero</p>
				</div>
				<div>
					<button className="buttonSuperHero" onClick={() => deleteProfil(profil.id)}>🔥 Remove</button>
					<button className="buttonSuperHero" /* onClick={} */>✏️ Edit</button>
				</div>
			</div>
			<div className="containerProfil">
				<div className="cardSuperHeroeProfil">
					<img className="imgSuperHeroeProfil" src={profil.picturePath} alt="Logo" width="300px" />
				</div>
				<div className="containerInfoProfil">
					<div className="profilName">
						<p>{profil.name}</p>
					</div>
					<div className="containerXPLOCATION">
						<div className="containerXPLOCATIONCOLUMN">
							<div><p>XP</p></div>
							<div className="detailXPLOCATION"><p>{profil.xp} years</p></div>
						</div>
						<div className="containerXPLOCATIONCOLUMN">
							<div><p>LOCATION</p></div>
							<div className="detailXPLOCATION"><p>{profil.location}</p></div>
						</div>
					</div>
					<div>
						<p>{profil.description}</p>
					</div>
					<div>
						<p>SKILLS</p>
						{profil.skills.map((data: any) => {
							return (
								<p key={data}>{data}</p>
							)
						})}
					</div>
				</div>



			</div>

			<div className="containerBotSomeProfil">
				<p className="labelSomeProfil">Check out other profiles</p>
				<div className="containerSomeSuperHeroes">
					{someSuperHeroeProfil.map((data: any) => {
						return (
							<div key={data.name} className="cardSomeSuperHeroeProfil" onClick={() => setViewProfil(data)}>
								<img className="imgSomeSuperHeroeProfil" src={data.picturePath} alt="Logo" width="150px" />
								<div className="someSuperHeroeLabel">
									<p className="someSuperHeroeLabelNameProfil" key={data.name}>
										{data.name}
									</p>
								</div>
							</div>
						)
					})}
				</div>
			</div>
		</>
	);
};

export default Profil;
