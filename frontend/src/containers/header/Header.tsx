import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import "./styles.css";


const Header = () => {
	const [SUPER] = useState("SUPER");
	const [HEROES] = useState("HEROES");
	let history = useHistory();

	function redirectHome () {
		history.push('/home');
	}

	return (
		<>
			<header className="test">
				<p className="header">{SUPER}</p>
				<img className="logoOffWorks" onClick={redirectHome} src='favicon.png' alt="Logo" width="50px"/>
				<p className="header">{HEROES}</p>
			</header>
		</>
	);
};

export default Header;