import React, { useEffect, useState } from "react";
import { useHistory } from 'react-router-dom';
import "./styles.css";
import superheroes from '../superheroes/superheroes.json';
import emojis from '../superheroes/skills.json';
import axios from 'axios';

interface SuperHeroes {
	name: string;
	xp: number;
	location: string;
	picturePath: string;
	description: string;
	skills: [string];
}

const Home = () => {
	let [allSuperHeroes, setallSuperHeroes] = useState<SuperHeroes[]>([]);
	let history = useHistory();

	useEffect(() => {
		axios({
			url: 'http://localhost:3000/graphql',
			method: 'post',
			data: {
				query: `
			  query {
				findAll {
					id,
				  name,
				  location,
				  xp,
				  skills,
				  picturePath,
				  description
				}
			  }
				`
			}
		}).then((result) => {
			setallSuperHeroes(result.data.data.findAll);
		});
	}, [])


	function redirectCreate() {
		history.push('/create');
	}

	function redirectViewProfil(data: {}) {
		history.push({
			pathname: '/profil',
			state: data,
		});
	}

	return (
		<>
			<div className="navBar">
				<div className="pathNavBar">
					<p className="Home">Home / </p>
					<p className="Path">🦸‍♀️🦸‍♂️Search</p>
				</div>
				<div>
					<button className="buttonSuperHero" onClick={redirectCreate}>⚙️ add superhero</button>
				</div>
			</div>
			<div className="searchFilterBar">
				<input className="inputSearch" type="text" id="name" name="name" placeholder="Batman, Gotham, Power Suit, etc" />

				<select name="cars" id="cars" className="dropdownFilter">
					<option value="test">test</option>
					<option value="test">test</option>
					<option value="test">test</option>
					<option value="test">test</option>
				</select>

				<button className="buttonSearch">Search</button>
			</div>

			<div className="listCardSuperHeroe">
				{allSuperHeroes.map((data, key) => {
					return (
						<div key={key} className="cardSuperHeroeHome" onClick={() => redirectViewProfil(data)}>
							<img className="imgSuperHeroe" key={data.picturePath} src={data.picturePath} alt="Logo" width="300px" /> {/*  width="50px"  */}
							<div className="superHeroeLabel">
								<p className="superHeroeLabelName" key={data.name}>
									{data.name}
								</p>
								<p className="superHeroeLabelSkills">
									{data.skills.map((data, key) => {
										const result = emojis.filter(skill => {
											if (skill.name === data) {
												return skill
											}
											return "";
										})
										return result[0] ? result[0].emoji : "";
									})}
								</p>
							</div>
						</div>
					)
				})}
			</div>

		</>
	);
};

export default Home;