import { hot } from "react-hot-loader/root";
import React from "react";
import { Switch, Route, Redirect } from "react-router";
import { useLocation } from "react-router";
import { ApolloProvider } from "@apollo/client";
import { client } from "./graphql";
import { Home, Profil, Create, Edit } from "#/containers";

const App = () => {
	const location = useLocation();

	return (
		<ApolloProvider client={client}>
			<Switch location={location}>

				<Route exact path="/">
					<Redirect to="/home" />
				</Route>
				
				<Route path="/home" component={Home} />

				<Route path="/profil" component={Profil} />

				<Route path="/create" component={Create} />

				<Route path="/edit" component={Edit} />

			</Switch>
		</ApolloProvider>
	);
};

export default hot(App);
