/* eslint-disable @typescript-eslint/array-type */
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
	[K in keyof T]: T[K];
};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
	ID: string;
	String: string;
	Boolean: boolean;
	Int: number;
	Float: number;
};

export type Mutation = {
	__typename?: "Mutation";
	/** Return "hello world 👋 (mutation)" */
	helloWorldMutation: Scalars["String"];
};

export type Query = {
	__typename?: "Query";
	/** Return "hello world 👋 (query)" */
	helloWorldQuery: Scalars["String"];
};
