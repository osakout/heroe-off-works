/* eslint-disable */
// This file was automatically generated and should not be edited.
// Run "npm run graphql:tools" to update this file
import * as Types from "../types/types";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
export type HelloWorldQueryQueryVariables = Types.Exact<{
	[key: string]: never;
}>;

export type HelloWorldQueryQuery = { __typename?: "Query" } & Pick<
	Types.Query,
	"helloWorldQuery"
>;

export const HelloWorldQueryDocument = gql`
	query helloWorldQuery {
		helloWorldQuery
	}
`;

/**
 * __useHelloWorldQueryQuery__
 *
 * To run a query within a React component, call `useHelloWorldQueryQuery` and pass it any options that fit your needs.
 * When your component renders, `useHelloWorldQueryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useHelloWorldQueryQuery({
 *   variables: {
 *   },
 * });
 */
export function useHelloWorldQueryQuery(
	baseOptions?: Apollo.QueryHookOptions<
		HelloWorldQueryQuery,
		HelloWorldQueryQueryVariables
	>
) {
	return Apollo.useQuery<HelloWorldQueryQuery, HelloWorldQueryQueryVariables>(
		HelloWorldQueryDocument,
		baseOptions
	);
}
export function useHelloWorldQueryLazyQuery(
	baseOptions?: Apollo.LazyQueryHookOptions<
		HelloWorldQueryQuery,
		HelloWorldQueryQueryVariables
	>
) {
	return Apollo.useLazyQuery<
		HelloWorldQueryQuery,
		HelloWorldQueryQueryVariables
	>(HelloWorldQueryDocument, baseOptions);
}
export type HelloWorldQueryQueryHookResult = ReturnType<
	typeof useHelloWorldQueryQuery
>;
export type HelloWorldQueryLazyQueryHookResult = ReturnType<
	typeof useHelloWorldQueryLazyQuery
>;
export type HelloWorldQueryQueryResult = Apollo.QueryResult<
	HelloWorldQueryQuery,
	HelloWorldQueryQueryVariables
>;
