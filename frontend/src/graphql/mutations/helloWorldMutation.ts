/* eslint-disable */
// This file was automatically generated and should not be edited.
// Run "npm run graphql:tools" to update this file
import * as Types from "../types/types";

import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
export type HelloWorldMutationMutationVariables = Types.Exact<{
	[key: string]: never;
}>;

export type HelloWorldMutationMutation = { __typename?: "Mutation" } & Pick<
	Types.Mutation,
	"helloWorldMutation"
>;

export const HelloWorldMutationDocument = gql`
	mutation helloWorldMutation {
		helloWorldMutation
	}
`;
export type HelloWorldMutationMutationFn = Apollo.MutationFunction<
	HelloWorldMutationMutation,
	HelloWorldMutationMutationVariables
>;

/**
 * __useHelloWorldMutationMutation__
 *
 * To run a mutation, you first call `useHelloWorldMutationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useHelloWorldMutationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [helloWorldMutationMutation, { data, loading, error }] = useHelloWorldMutationMutation({
 *   variables: {
 *   },
 * });
 */
export function useHelloWorldMutationMutation(
	baseOptions?: Apollo.MutationHookOptions<
		HelloWorldMutationMutation,
		HelloWorldMutationMutationVariables
	>
) {
	return Apollo.useMutation<
		HelloWorldMutationMutation,
		HelloWorldMutationMutationVariables
	>(HelloWorldMutationDocument, baseOptions);
}
export type HelloWorldMutationMutationHookResult = ReturnType<
	typeof useHelloWorldMutationMutation
>;
export type HelloWorldMutationMutationResult = Apollo.MutationResult<
	HelloWorldMutationMutation
>;
export type HelloWorldMutationMutationOptions = Apollo.BaseMutationOptions<
	HelloWorldMutationMutation,
	HelloWorldMutationMutationVariables
>;
