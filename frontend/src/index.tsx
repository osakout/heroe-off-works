import React             from "react";
import ReactDOM          from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Header, Footer } from "#/containers";
import App               from "./App";
import "./styles.css";

ReactDOM.render(
	<BrowserRouter>
		<React.Suspense fallback="">
			<Header/>
			<App />
			<Footer/>
		</React.Suspense>
	</BrowserRouter>, document.getElementById("root")
);
