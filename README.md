# Projet

Super Heroes OFF-WORKS

### Prérequis

Installer :

* Postgresql
* NodeJS

Une base de données Postgresql doit être disponible sur le port par default 5432

**Nom base de donnée:** superhero_test_test

**Port:** 5432

Modifier le nom du user de la base de donnée dans ./back-api/app/models/index.js l.3 à la place de "omarsakout"

```
let sequelize = new Sequelize('superhero_test_test', 'omarsakout', '', {
```

/!\ À chaque lancement du serveur back, la base de donnée est réinitialiser

### Lancer le projet

Pour lancer le projet, installez  les dépendances et lancer chaque service avec la commande npm start.

# Frontend
```
cd frontend/
npm i 
npm start
```

# Backend
```
cd back-api/
npm i
npm start
```

# Améliorer le projet

### Backend

Pour la liste des héros, un système de pagination peut être mis en place si il y avait eu beaucoup de données pour éviter d'avoir des appels back avec un temp de réponse lent.

Le système de recherche et de filtre doit être gérer de préfèrence côté back pour avoir un résultat optimale, imaginons qu'entre temps où le client a demandé la liste de tous les super héros, et le temps où il fait une recherche ciblé, un super héro a été créer, il ne pourra pas y avoir accès.

Vérification des bonnes données reçu.

Modification de profil.

### Frontend

Modification de profil.

Design responsive, actuellement le design est "nativement" responsive grâce au système flexbox, le design aurait pu être totalement responsive grâce aux media queries en css.

